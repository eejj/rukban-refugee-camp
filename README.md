# Background

- Read this for details: https://medium.com/@eejj/visualizing-the-refugee-crisis-at-the-syrian-jordanian-border-70526b60fb12

# Data sources

- UNOSAT data:
    - http://www.unitar.org/unosat/node/44/2824
    - http://www.unitar.org/unosat/node/44/2761
    - http://www.unitar.org/unosat/node/44/2680
    - http://www.unitar.org/unosat/node/44/2618
    - http://www.unitar.org/unosat/node/44/2586
    - http://www.unitar.org/unosat/node/44/2549
    - http://www.unitar.org/unosat/node/44/2503
    - http://www.unitar.org/unosat/node/44/2486
    - http://www.unitar.org/unosat/node/44/2449
    - http://www.unitar.org/unosat/node/44/2421
    - http://www.unitar.org/unosat/node/44/2416
    - http://www.unitar.org/unosat/node/44/2412
    - http://www.unitar.org/unosat/node/44/2403

- Sentinel-2 satellite images: See `2_sentinel2_keys.json` for a list of keys.

- HOTOSM Syria road network: https://data.humdata.org/dataset/hotosm_syr_roads

# Source code

- `0_shapefile_to_csv.ipynb`: converts UNOSAT's data from shapefile to csv file format.

- `1_shelter_density_map_maker.ipynb`: makes a shelter density map given a csv file.

- `2_fetch_raster.py`: downloads Red, Blue and Green bands of the listed Sentinel-2 products in `2_sentinel2_keys.json` from AWS. It costs around $0.3 to download all imagery.

- `3_process_raster.ipynb`: processes the downloaded Sentinel-2 imagery and makes it ready for visualization (Figure 3).