#This script downloads Sentinel-2 rasters (Red, Green & Blue bands only) listed in `2_sentinel2_keys.json`.
#You need to setup your AWS credentials from sommand line before running this script.

import os
import json
import boto3

#AWS s3 setup
BUCKET_NAME = 'sentinel-s2-l1c'
s3 = boto3.resource('s3')

#setting paths
json_path  = '2_sentinel2_keys.json'
write_path = '../data/raster'

if not os.path.exists(write_path):
    os.mkdir(write_path)

rasters = json.load(open(json_path))

for raster in rasters:
    for key in rasters[raster]:

        print('Downloading raster: {}'.format(raster))

        #make directory for each product
        raster_dir = os.path.join(write_path, raster)
        if not os.path.exists(raster_dir):
            os.mkdir(raster_dir)

        #download B, G, and R bands
        band_02_key = os.path.join(key, 'B02.jp2')
        band_03_key = os.path.join(key, 'B03.jp2')
        band_04_key = os.path.join(key, 'B04.jp2')

        band_02_out_path = os.path.join(raster_dir, 'B02.jp2')
        band_03_out_path = os.path.join(raster_dir, 'B03.jp2')
        band_04_out_path = os.path.join(raster_dir, 'B04.jp2')

        s3.Bucket(BUCKET_NAME).download_file(band_02_key, band_02_out_path, {'RequestPayer':'requester'})
        s3.Bucket(BUCKET_NAME).download_file(band_03_key, band_03_out_path, {'RequestPayer':'requester'})
        s3.Bucket(BUCKET_NAME).download_file(band_04_key, band_04_out_path, {'RequestPayer':'requester'})
